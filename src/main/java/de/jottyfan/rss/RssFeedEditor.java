package de.jottyfan.rss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 
 * @author jotty
 *
 */
@SpringBootApplication
@EnableTransactionManagement
public class RssFeedEditor extends SpringBootServletInitializer {
  
	@Override
  protected SpringApplicationBuilder configure(
    SpringApplicationBuilder application) {
    return application.sources(RssFeedEditor.class);
  }

	public static void main(String[] args) {
		SpringApplication.run(RssFeedEditor.class, args);
	}
}
