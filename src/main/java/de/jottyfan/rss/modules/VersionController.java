package de.jottyfan.rss.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * 
 * @author jotty
 *
 */
@Controller
public class VersionController {

	@Autowired
	private ManifestBean bean;
	
	@ModelAttribute("version")
	public String getVersion() {
		return bean.getVersion();
	}
}
