package de.jottyfan.rss.modules.provider;

import static de.jottyfan.RssFeedEditor.db.Tables.T_CHANNEL;
import static de.jottyfan.RssFeedEditor.db.Tables.V_RSS;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.rss.modules.editor.ChannelBean;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class ProviderRepository {

	private final static Logger LOGGER = LogManager.getLogger(ProviderRepository.class);

	@Autowired
	private DSLContext jooq;

	public List<ChannelBean> getFeeds() {
		SelectJoinStep<Record3<Integer, String, String>> sql = jooq
		// @formatter:off
			.select(T_CHANNEL.PK,
							T_CHANNEL.TITLE,
							T_CHANNEL.DESCRIPTION)
			.from(T_CHANNEL);
		// @formatter:on
		LOGGER.trace(sql.toString());
		List<ChannelBean> list = new ArrayList<>();
		for (Record3<Integer, String, String> r : sql.fetch()) {
			ChannelBean bean = new ChannelBean();
			bean.setPk(r.get(T_CHANNEL.PK));
			bean.setTitle(r.get(T_CHANNEL.TITLE));
			bean.setDescription(r.get(T_CHANNEL.DESCRIPTION));
			list.add(bean);
		}
		return list;
	}

	public String getFeed(String feedname) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(V_RSS.FEED)
			.from(V_RSS)
			.where(V_RSS.FEEDNAME.eq(feedname));
		// @formatter:on
		LOGGER.trace(sql.toString());
		return sql.fetchOne(V_RSS.FEED);
	}
}
