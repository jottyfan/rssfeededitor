package de.jottyfan.rss.modules.provider;

import java.io.Serializable;

/**
 * 
 * @author jotty
 *
 */
public class RssBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String feedname;
	private String feed;

	/**
	 * @return the feedname
	 */
	public String getFeedname() {
		return feedname;
	}

	/**
	 * @param feedname the feedname to set
	 */
	public void setFeedname(String feedname) {
		this.feedname = feedname;
	}

	/**
	 * @return the feed
	 */
	public String getFeed() {
		return feed;
	}

	/**
	 * @param feed the feed to set
	 */
	public void setFeed(String feed) {
		this.feed = feed;
	}
}
