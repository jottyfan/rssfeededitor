package de.jottyfan.rss.modules.provider;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.jottyfan.rss.modules.VersionController;

/**
 * 
 * @author jotty
 *
 */
@Controller
public class ProviderController extends VersionController {

	@Autowired
	private ProviderService service;
	
	@GetMapping("/")
	public String getFeeds(Model model) {
		model.addAttribute("feeds", service.getFeeds());
		return "/index";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}
	
	@RequestMapping(value = "/public/{feedname}", method = RequestMethod.GET, produces = { "application/rss+xml" }, consumes = MediaType.ALL_VALUE )
  @ResponseBody
	public String getFeeds(@PathVariable String feedname, Model model) {
		return service.getFeed(feedname);
	}
}
