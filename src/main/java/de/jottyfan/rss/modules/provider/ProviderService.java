package de.jottyfan.rss.modules.provider;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.jottyfan.rss.modules.editor.ChannelBean;

/**
 * 
 * @author jotty
 *
 */
@Service
public class ProviderService {

	@Autowired
	private ProviderRepository repository;

	public List<ChannelBean> getFeeds() {
		return repository.getFeeds();
	}

	public String getFeed(String feedname) {
		return repository.getFeed(feedname);
	}
}
