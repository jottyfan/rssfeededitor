package de.jottyfan.rss.modules;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * @author jotty
 *
 */
@Component
public class ManifestBean {

    @Autowired(required = false)
    private BuildProperties buildProperties;

    public String getVersion() {
        return  buildProperties != null ? buildProperties.getVersion() : "0.0.0";
    }
}
