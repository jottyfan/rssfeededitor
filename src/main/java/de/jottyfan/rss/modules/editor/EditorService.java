package de.jottyfan.rss.modules.editor;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jotty
 *
 */
@Service
public class EditorService {
	
	@Autowired
	private EditorRepository repository;

	public List<ChannelBean> getChannels() {
		return repository.getChannels();
	}

	public ChannelBean getChannel(Integer channel) {
		return repository.getChannel(channel);
	}

	public void updateChannel(ChannelBean bean) {
		repository.updateChannel(bean);
	}

	public List<ItemBean> getItems(Integer channel) {
		return repository.getItems(channel);
	}

	public ItemBean getItem(Integer id) {
		return repository.getItem(id);
	}

	public void updateItem(ItemBean bean) {
		repository.updateItem(bean);
	}

	public Integer addEmptyItem(Integer channel) {
		return repository.addEmptyItem(channel);
	}
	
	public void deleteItem(Integer pk) {
		repository.deleteItem(pk);
	}

	public Integer addChannel() {
		return repository.addChannel();
	}

	public void deleteChannel(Integer channel) {
		repository.deleteChannel(channel);
	}
}
