package de.jottyfan.rss.modules.editor;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import de.jottyfan.RssFeedEditor.db.tables.records.TItemRecord;

/**
 * 
 * @author jotty
 *
 */
public class ItemBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer pk;
	private Integer fkChannel;
	private String title;
	private String description;
	private String link;
	private String author;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime pubdate;

	public static final ItemBean of(TItemRecord r) {
		if (r != null) {
			ItemBean bean = new ItemBean();
			bean.setPk(r.getPk());
			bean.setFkChannel(r.getFkChannel());
			bean.setTitle(r.getTitle());
			bean.setDescription(r.getDescription());
			bean.setLink(r.getLink());
			bean.setAuthor(r.getAuthor());
			bean.setPubdate(r.getPubdate());
			return bean;
		} else {
			return null;
		}
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the fkChannel
	 */
	public Integer getFkChannel() {
		return fkChannel;
	}

	/**
	 * @param fkChannel the fkChannel to set
	 */
	public void setFkChannel(Integer fkChannel) {
		this.fkChannel = fkChannel;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

	/**
	 * @return the pubdate
	 */
	public LocalDateTime getPubdate() {
		return pubdate;
	}

	/**
	 * @param pubdate the pubdate to set
	 */
	public void setPubdate(LocalDateTime pubdate) {
		this.pubdate = pubdate;
	}
}
