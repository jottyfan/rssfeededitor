package de.jottyfan.rss.modules.editor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import de.jottyfan.rss.modules.VersionController;

/**
 * 
 * @author jotty
 *
 */
@Controller
public class EditorController extends VersionController {
	
	@Autowired
	private EditorService service;


	@GetMapping("/editor/list")
	public String getList(Model model) {
		model.addAttribute("channels", service.getChannels());
		return "/editor/list";
	}
	
	@GetMapping("/editor/add")
	public String addChannel(Model model) {
		Integer pk = service.addChannel();
		return "redirect:/editor/" + pk + "/edit";
	}
	
	@GetMapping("/editor/{channel}/edit")
	public String getEditChannel(@PathVariable Integer channel, Model model) {
		model.addAttribute("bean", service.getChannel(channel));
		model.addAttribute("items", service.getItems(channel));
		return "/editor/channel";
	}
	
	@GetMapping("/editor/{channel}/delete")
	public String getEditChannel(@PathVariable Integer channel) {
		service.deleteChannel(channel);
		return "redirect:/editor/list";
	}
	
	
	@PostMapping("/editor/channel/{id}/update")
	public String updateChannel(@ModelAttribute ChannelBean bean) throws Exception {
		service.updateChannel(bean);
		return "redirect:/editor/list";
	}
	
	@GetMapping("/editor/{channel}/feed/{id}/edit")
	public String getEditItem(@PathVariable Integer channel, @PathVariable Integer id, Model model) {
		model.addAttribute("bean", service.getItem(id));
		return "/editor/item";
	}
	
	@GetMapping("/editor/{channel}/feed/add")
	public String addItem(@PathVariable Integer channel, Model model) {
		Integer fkItem = service.addEmptyItem(channel);
		return "redirect:/editor/" + channel + "/feed/" + fkItem + "/edit";
	}
	
	@PostMapping("/editor/item/{id}/update")
	public String updateItem(@ModelAttribute ItemBean bean) throws Exception {
		service.updateItem(bean);
		return "redirect:/editor/" + bean.getFkChannel() + "/edit";
	}
	
	@GetMapping("/editor/{channel}/feed/{id}/delete")
	public String deleteItem(@PathVariable Integer channel, @PathVariable Integer id, Model model) {
		service.deleteItem(id);
		return "redirect:/editor/" + channel + "/edit";
	}
}
