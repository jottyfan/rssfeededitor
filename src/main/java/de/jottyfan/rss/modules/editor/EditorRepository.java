package de.jottyfan.rss.modules.editor;

import static de.jottyfan.RssFeedEditor.db.Tables.T_CHANNEL;
import static de.jottyfan.RssFeedEditor.db.Tables.T_ITEM;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.Field;
import org.jooq.InsertResultStep;
import org.jooq.Record;
import org.jooq.Record7;
import org.jooq.Record8;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.UpdateConditionStep;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.RssFeedEditor.db.tables.records.TChannelRecord;
import de.jottyfan.RssFeedEditor.db.tables.records.TItemRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class EditorRepository {
	private static final Logger LOGGER = LogManager.getLogger(EditorRepository.class);

	@Autowired
	private DSLContext jooq;

	public List<ChannelBean> getChannels() {
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		SelectHavingStep<Record8<Integer, String, String, String, String, String, LocalDateTime, Integer>> sql = jooq
		// @formatter:off
			.select(T_CHANNEL.PK,
							T_CHANNEL.TITLE,
							T_CHANNEL.DESCRIPTION,
							T_CHANNEL.LINK,
							T_CHANNEL.LANGUAGE,
							T_CHANNEL.COPYRIGHT,
							T_CHANNEL.PUBDATE,
							DSL.count(T_ITEM.PK).as(AMOUNT))
			.from(T_CHANNEL)
			.leftJoin(T_ITEM).on(T_ITEM.FK_CHANNEL.eq(T_CHANNEL.PK))
			.groupBy(T_CHANNEL.PK, T_CHANNEL.TITLE, T_CHANNEL.DESCRIPTION, T_CHANNEL.LINK, T_CHANNEL.LANGUAGE, T_CHANNEL.COPYRIGHT, T_CHANNEL.PUBDATE);
		// @formatter:on
		LOGGER.trace(sql.toString());
		List<ChannelBean> list = new ArrayList<>();
		for (Record r : sql.fetch()) {
			ChannelBean bean = new ChannelBean();
			bean.setPk(r.get(T_CHANNEL.PK));
			bean.setTitle(r.get(T_CHANNEL.TITLE));
			bean.setDescription(r.get(T_CHANNEL.DESCRIPTION));
			bean.setLink(r.get(T_CHANNEL.LINK));
			bean.setLanguage(r.get(T_CHANNEL.LANGUAGE));
			bean.setCopyright(r.get(T_CHANNEL.COPYRIGHT));
			LocalDateTime ldt = r.get(T_CHANNEL.PUBDATE);
			bean.setPubdate(ldt == null ? null : ldt.toLocalDate());
			bean.setItemSize(r.get(AMOUNT));
			list.add(bean);
		}
		return list;
	}

	public ChannelBean getChannel(Integer channel) {
		SelectConditionStep<Record7<Integer, String, String, String, String, String, LocalDateTime>> sql = jooq
		// @formatter:off
			.select(T_CHANNEL.PK,
							T_CHANNEL.TITLE,
							T_CHANNEL.DESCRIPTION,
							T_CHANNEL.LINK,
							T_CHANNEL.LANGUAGE,
							T_CHANNEL.COPYRIGHT,
							T_CHANNEL.PUBDATE)
			.from(T_CHANNEL)
			.where(T_CHANNEL.PK.eq(channel));
		// @formatter:on
		LOGGER.trace(sql.toString());
		Record r = sql.fetchOne();
		if (r != null) {
			ChannelBean bean = new ChannelBean();
			bean.setPk(r.get(T_CHANNEL.PK));
			bean.setTitle(r.get(T_CHANNEL.TITLE));
			bean.setDescription(r.get(T_CHANNEL.DESCRIPTION));
			bean.setLink(r.get(T_CHANNEL.LINK));
			bean.setLanguage(r.get(T_CHANNEL.LANGUAGE));
			bean.setCopyright(r.get(T_CHANNEL.COPYRIGHT));
			LocalDateTime ldt = r.get(T_CHANNEL.PUBDATE);
			bean.setPubdate(ldt == null ? null : ldt.toLocalDate());
			return bean;
		} else {
			return null;
		}
	}

	public void updateChannel(ChannelBean bean) {
		UpdateConditionStep<TChannelRecord> sql = jooq
		// @formatter:off
			.update(T_CHANNEL)
			.set(T_CHANNEL.TITLE, bean.getTitle())
			.set(T_CHANNEL.DESCRIPTION, bean.getDescription())
			.set(T_CHANNEL.LINK, bean.getLink())
			.set(T_CHANNEL.LANGUAGE, bean.getLanguage())
			.set(T_CHANNEL.COPYRIGHT, bean.getCopyright())
			.set(T_CHANNEL.PUBDATE, bean.getPubdate() == null ? null : bean.getPubdate().atStartOfDay())
			.where(T_CHANNEL.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	public List<ItemBean> getItems(Integer channel) {
		SelectConditionStep<TItemRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_ITEM)
			.where(T_ITEM.FK_CHANNEL.eq(channel));
		// @formatter:on
		LOGGER.trace(sql.toString());
		List<ItemBean> list = new ArrayList<>();
		for (TItemRecord r : sql.fetch()) {
			ItemBean bean = ItemBean.of(r);
			if (bean != null) {
				list.add(bean);
			}
		}
		return list;
	}

	public ItemBean getItem(Integer id) {
		SelectConditionStep<TItemRecord> sql = jooq
		// @formatter:off
			.selectFrom(T_ITEM)
			.where(T_ITEM.PK.eq(id));
		// @formatter:on
		LOGGER.trace(sql.toString());
		TItemRecord r = sql.fetchOne();
		return r == null ? null : ItemBean.of(r);
	}

	public void updateItem(ItemBean bean) {
		UpdateConditionStep<TItemRecord> sql = jooq
		// @formatter:off
			.update(T_ITEM)
			.set(T_ITEM.TITLE, bean.getTitle())
			.set(T_ITEM.DESCRIPTION, bean.getDescription())
			.set(T_ITEM.LINK, bean.getLink())
			.set(T_ITEM.AUTHOR, bean.getAuthor())
			.set(T_ITEM.PUBDATE, bean.getPubdate())
			.where(T_ITEM.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	public Integer addEmptyItem(Integer channel) {
		InsertResultStep<TItemRecord> sql = jooq
		// @formatter:off
			.insertInto(T_ITEM, 
					        T_ITEM.FK_CHANNEL,
					        T_ITEM.TITLE,
					        T_ITEM.DESCRIPTION,
					        T_ITEM.PUBDATE)
			.values(channel, "", "", LocalDateTime.now())
			.returning(T_ITEM.PK);
		// @formatter:on
		LOGGER.trace(sql.toString());
		return sql.fetchOne().getPk();
	}

	public void deleteItem(Integer pk) {
		DeleteConditionStep<TItemRecord> sql = jooq.deleteFrom(T_ITEM).where(T_ITEM.PK.eq(pk));
		LOGGER.trace(sql.toString());
		sql.execute();
	}

	public Integer addChannel() {
		InsertResultStep<TChannelRecord> sql = jooq
		// @formatter:off
			.insertInto(T_CHANNEL,
					        T_CHANNEL.TITLE,
					        T_CHANNEL.DESCRIPTION,
					        T_CHANNEL.LINK,
					        T_CHANNEL.PUBDATE)
			.values("", "", "", LocalDateTime.now())
			.returning(T_CHANNEL.PK);
		// @formatter:on
		LOGGER.trace(sql.toString());
		return sql.fetchOne().getPk();
	}

	public void deleteChannel(Integer channel) {
		jooq.transaction(t -> {
			DeleteConditionStep<TItemRecord> sql0 = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_ITEM)
				.where(T_ITEM.FK_CHANNEL.eq(channel));
			// @formatter:on
			LOGGER.trace(sql0.toString());
			sql0.execute();
			
			DeleteConditionStep<TChannelRecord> sql1 = DSL.using(t)
			// @formatter:off
				.deleteFrom(T_CHANNEL)
				.where(T_CHANNEL.PK.eq(channel));
			// @formatter:on
			LOGGER.trace(sql1.toString());
			sql1.execute();
		});
	}
}
